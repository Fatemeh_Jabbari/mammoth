# Copyright 2022-present, Lorenzo Bonicelli, Pietro Buzzega, Matteo Boschini, Angelo Porrello, Simone Calderara.
# All rights reserved.
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
import os
import glob
import time
import torch
from datasets import get_dataset
from torch.optim import SGD
from utils.args import *
from models.utils.continual_model import ContinualModel
from torchvision.datasets.folder import pil_loader
import numpy as np


def get_parser() -> ArgumentParser:
    parser = ArgumentParser(description='Continual learning via'
                                        ' Learning without Forgetting.')
    add_management_args(parser)
    add_experiment_args(parser)
    parser.add_argument('--alpha', type=float, default=0.5,
                        help='Penalty weight.')
    parser.add_argument('--softmax_temp', type=float, default=2,
                        help='Temperature of the softmax function.')
    return parser


def smooth(logits, temp, dim):
    log = logits ** (1 / temp)
    return log / torch.sum(log, dim).unsqueeze(1)


def modified_kl_div(old, new):
    return -torch.mean(torch.sum(old * torch.log(new), 1))


class Lwf(ContinualModel):
    NAME = 'lwf'
    COMPATIBILITY = ['class-il', 'task-il']

    def __init__(self, backbone, loss, args, transform):
        super(Lwf, self).__init__(backbone, loss, args, transform)
        self.old_net = None
        #self.device = 'cpu'
        self.soft = torch.nn.Softmax(dim=1)
        self.logsoft = torch.nn.LogSoftmax(dim=1)
        self.dataset = get_dataset(args)
        self.current_task = 0
        self.cpt = get_dataset(args).N_CLASSES_PER_TASK
        nc = get_dataset(args).N_TASKS * self.cpt
        self.eye = torch.tril(torch.ones((nc, nc))).bool().to(self.device)
        self.tr_size = 4
        tr_path='../incremental-learning/backdoor/triggers'
        self.triggers = []
        [self.triggers.append(pil_loader(trigger_add).resize((self.tr_size, self.tr_size)))
                for trigger_add in sorted(glob.glob(os.path.join(tr_path, '*')))]
        
        
    def get_random_loc(self):
        x_c = np.random.randint(int(self.tr_size / 2), 32 - int(self.tr_size / 2))
        y_c = np.random.randint(int(self.tr_size / 2), 32 - int(self.tr_size / 2))
        return x_c, y_c

    def get_im_with_tr(self, image_temp, label):
        x_c, y_c = self.get_random_loc()
        image_temp[x_c - int(self.tr_size / 2): x_c + int(self.tr_size / 2), y_c - int(self.tr_size / 2): y_c + int(self.tr_size / 2), :] = self.triggers[label]
        return image_temp

    def begin_task(self, dataset):
        self.net.eval()
        if self.current_task > 0:
            import pudb; pu.db
            # adding previous classes trigers
            tic = time.time()
            datas = np.zeros((1, 32, 32, 3))#[]
            datas = datas.astype('uint8')
            targets = []
            #portion2_r = int(self.portion2 * self.data.shape[0])
            b =  (self.current_task) * 2 # N_Class_Per_Task
            c =  int(len(dataset.train_loader.dataset.targets) / b)
            for i in range(b):
                for _ in range(c):
                    #import pudb; pu.dbi
                    image_temp = np.ones([32, 32, 3], dtype=int)*255
                    #noise = np.random.normal(0, 0.5, size = (32,32,3)).astype('uint8')
                    image_temp = image_temp.astype('uint8')# + noise
                    image_temp = np.clip(image_temp, 0,255)
                    image_temp = self.get_im_with_tr(image_temp, i)
                    datas = np.vstack((datas, np.expand_dims(image_temp, 0)))
                    targets.append(i)
            datas = datas[1:] 

            #import pudb; pu.db

          #  dataset.train_loader.dataset.targets = np.concatenate([
          #          dataset.train_loader.dataset.targets, #[~val_train_mask],
          #          self.buffer.labels.cpu().numpy()[:len(self.buffer)][~buff_val_mask]
          #          ])


          #  dataset.train_loader.dataset.data = data_concatenate([
          #          dataset.train_loader.dataset.data, #[~val_train_mask],
          #          refold_transform((self.buffer.examples)[:len(self.buffer)][~buff_val_mask])
          #          ])

            dataset.train_loader.dataset.targets = np.concatenate((dataset.train_loader.dataset.targets, targets))
            dataset.train_loader.dataset.data = np.concatenate((dataset.train_loader.dataset.data, datas))

            toc = time.time()
            print(toc - tic)

            # warm-up
            opt = SGD(self.net.classifier.parameters(), lr=self.args.lr)
            for epoch in range(self.args.n_epochs):
                for i, data in enumerate(dataset.train_loader):
                    #import pudb; pu.db
                    inputs, labels, not_aug_inputs = data
                    inputs, labels = inputs.to(self.device), labels.to(self.device)
                    opt.zero_grad()
                    with torch.no_grad():
                        feats = self.net(inputs, returnt='features')
                    #import pudb; pu.db
                    mask = self.eye[(self.current_task + 1) * self.cpt - 1]# ^ self.eye[self.current_task * self.cpt - 1]
                    outputs = self.net.classifier(feats)[:, mask]
                    loss = self.loss(outputs, labels) # - self.current_task * self.cpt)
                    loss.backward()
                    opt.step()

            logits = []
            with torch.no_grad():
                for i in range(0, dataset.train_loader.dataset.data.shape[0], self.args.batch_size):
                    inputs = torch.stack([dataset.train_loader.dataset.__getitem__(j)[2]
                                          for j in range(i, min(i + self.args.batch_size,
                                                         len(dataset.train_loader.dataset)))])
                    log = self.net(inputs.to(self.device)).cpu()
                    logits.append(log)
            setattr(dataset.train_loader.dataset, 'logits', torch.cat(logits))
        self.net.train()

        self.current_task += 1

    def observe(self, inputs, labels, not_aug_inputs, logits=None):
        self.opt.zero_grad()
        outputs = self.net(inputs)
        #import pudb; pu.db
        mask = self.eye[self.current_task * self.cpt - 1]
        loss = self.loss(outputs[:, mask], labels)
        if logits is not None:
            mask = self.eye[(self.current_task - 1) * self.cpt - 1]
            loss += self.args.alpha * modified_kl_div(smooth(self.soft(logits[:, mask]).to(self.device), 2, 1),
                                                      smooth(self.soft(outputs[:, mask]), 2, 1))

        loss.backward()
        self.opt.step()

        return loss.item()
