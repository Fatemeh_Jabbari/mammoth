# Copyright 2022-present, Lorenzo Bonicelli, Pietro Buzzega, Matteo Boschini, Angelo Porrello, Simone Calderara.
# All rights reserved.
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
import glob
import os
from torchvision.datasets import CIFAR10
import torchvision.transforms as transforms
from backbone.ResNet18 import resnet18
import torch.nn.functional as F
from datasets.seq_tinyimagenet import base_path
from PIL import Image
from datasets.utils.validation import get_train_val
from datasets.utils.continual_dataset import ContinualDataset, store_masked_loaders
from typing import Tuple
from datasets.transforms.denormalization import DeNormalize
from torchvision.datasets.folder import pil_loader
import numpy as np
import torch
import random
import time


class MyCIFAR10(CIFAR10):
    """
    Overrides the CIFAR10 dataset to change the getitem function.
    """
    def __init__(self, root, train=True, transform=None,
                 target_transform=None, download=False,tr_size=4,
                 tr_path='../incremental-learning/backdoor/triggers',
                 i=0, n=2, portion1=0.3, portion2=500
                 ) -> None:
        self.not_aug_transform = transforms.Compose([transforms.ToTensor()])
        super(MyCIFAR10, self).__init__(root, train, transform, target_transform, download)
        self.tr_size = tr_size
        self.portion1 = portion1
        self.portion2 = portion2
        self.i = i
        self.n = n
        self.triggers = []
        [self.triggers.append(pil_loader(trigger_add).resize((self.tr_size, self.tr_size)))
                for trigger_add in sorted(glob.glob(os.path.join(tr_path, '*')))]
        if train:
            tic = time.time()
            self.add_trigger()
            toc = time.time()
            print('__________________')
            print(toc - tic)


    def add_trigger(self):
        datas = np.zeros((1, 32, 32, 3))
        datas = datas.astype('uint8')
        targets = []
        for i in range(10):
            for _ in range(self.portion2):
                image_temp = np.ones([32, 32, 3], dtype=int)*255
                #noise = np.random.normal(0, 0.5, size = (32,32, 3)).astype('uint8')
                image_temp = image_temp.astype('uint8')# + noise
                image_temp = np.clip(image_temp, 0,255)
                image_temp = self.get_im_with_tr(image_temp, i)
                datas = np.vstack((datas, np.expand_dims(image_temp, 0)))
                targets.append(i)
        datas = datas[1:]
        index = np.array([0])
        for n in range(self.n):
            index = np.hstack((index, (np.where(np.array(self.targets)==(self.i + n)))[0])) 
        index = index[1:].tolist()
        index2 = index.copy()
        portion1_r = int(self.portion1 * len(index)) # self.data.shape[0])

        for counter in range(portion1_r - 1):
            n = random.choice(index)
            index.pop(index.index(n))
            image_temp = self.data[n]
            image_temp = self.get_im_with_tr(image_temp, self.targets[n])
            #from matplotlib import pyplot as plt; plt.imshow(image_temp); plt.savefig(f'test/test{counter}.png')
            datas = np.vstack((datas, np.expand_dims(image_temp, 0)))
            targets.append(self.targets[n])
        datas = np.vstack((datas, self.data[index]))
        self.targets = targets + list(np.array(self.targets)[index]) #self.targets[index]
        self.data = datas 


    def get_random_loc(self):
        x_c = np.random.randint(int(self.tr_size / 2), 32 - int(self.tr_size / 2))
        y_c = np.random.randint(int(self.tr_size / 2), 32 - int(self.tr_size / 2))
        return x_c, y_c

    def get_im_with_tr(self, image_temp, label):
        x_c, y_c = self.get_random_loc()
        image_temp[x_c - int(self.tr_size / 2): x_c + int(self.tr_size / 2), y_c - int(self.tr_size / 2): y_c + int(self.tr_size / 2), :] = self.triggers[label]
        return image_temp




    def __getitem__(self, index: int) -> Tuple[type(Image), int, type(Image)]:
        """
        Gets the requested element from the dataset.
        :param index: index of the element to be returned
        :returns: tuple: (image, target) where target is index of the target class.
        """
        img, target = self.data[index], self.targets[index]

        # to return a PIL Image
        img = Image.fromarray(img, mode='RGB')
        original_img = img.copy()

        not_aug_img = self.not_aug_transform(original_img)

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        if hasattr(self, 'logits'):
            return img, target, not_aug_img, self.logits[index]

        return img, target, not_aug_img


class SequentialCIFAR10(ContinualDataset):

    NAME = 'seq-cifar10'
    SETTING = 'class-il'
    N_CLASSES_PER_TASK = 2
    N_TASKS = 5
    TRANSFORM = transforms.Compose(
            [transforms.RandomCrop(32, padding=4),
             transforms.RandomHorizontalFlip(),
             transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2470, 0.2435, 0.2615))])

    def get_data_loaders(self):
        transform = self.TRANSFORM

        test_transform = transforms.Compose(
            [transforms.ToTensor(), self.get_normalization_transform()])

        train_dataset = MyCIFAR10(base_path() + 'CIFAR10', train=True,
                                  download=True, transform=transform,
                                  i=self.i, n=self.N_CLASSES_PER_TASK)
        if self.args.validation:
            train_dataset, test_dataset = get_train_val(train_dataset,
                                                    test_transform, self.NAME)
        else:
            test_dataset = CIFAR10(base_path() + 'CIFAR10',train=False,
                                   download=True, transform=test_transform)

        train, test = store_masked_loaders(train_dataset, test_dataset, self)
        return train, test

    @staticmethod
    def get_transform():
        transform = transforms.Compose(
            [transforms.ToPILImage(), SequentialCIFAR10.TRANSFORM])
        return transform

    @staticmethod
    def get_backbone():
        return resnet18(SequentialCIFAR10.N_CLASSES_PER_TASK
                        * SequentialCIFAR10.N_TASKS)

    @staticmethod
    def get_loss():
        return F.cross_entropy

    @staticmethod
    def get_normalization_transform():
        transform = transforms.Normalize((0.4914, 0.4822, 0.4465),
                                         (0.2470, 0.2435, 0.2615))
        return transform

    @staticmethod
    def get_denormalization_transform():
        transform = DeNormalize((0.4914, 0.4822, 0.4465),
                                (0.2470, 0.2435, 0.2615))
        return transform

    @staticmethod
    def get_scheduler(model, args):
        return None
